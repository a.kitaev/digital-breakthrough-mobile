package com.avocado.digital_breakthrough_mobile.ui.map

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.digital_breakthrough_mobile.R
import com.avocado.digital_breakthrough_mobile.databinding.FragmentMapBinding
import com.avocado.digital_breakthrough_mobile.databinding.ItemBottomSheetBinding
import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.avocado.digital_breakthrough_mobile.model.data.Place
import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace
import com.avocado.digital_breakthrough_mobile.ui.list.ListType
import com.avocado.digital_breakthrough_mobile.utils.observeNullable
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.material.bottomsheet.BottomSheetBehavior
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.io.Serializable

class MapFragment : Fragment(), OnMapReadyCallback, GoogleMap.OnMarkerClickListener {

    private val viewModel by viewModel<MapViewModel>()

    private lateinit var map: Map
    private lateinit var binding: FragmentMapBinding
    private lateinit var bottomSheet: BottomSheetBehavior<View>
    private lateinit var bottomSheetBinding: ItemBottomSheetBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadContent()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMapBinding.inflate(inflater)
        bottomSheetBinding = ItemBottomSheetBinding.inflate(
            layoutInflater,
            binding.bottomSheetContainer,
            true
        )
        initUI()
        initObservers()
        return binding.root
    }

    private fun initUI() {
        map = MapController(this)
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map_fragment) as SupportMapFragment
        mapFragment.getMapAsync(this)
        binding.cardButton.setOnClickListener {
            findNavController().navigateUp()
        }
        bottomSheet = BottomSheetBehavior.from(bottomSheetBinding.root)
    }

    private fun initObservers() = with(viewModel) {
        markerLiveData.observeNullable(viewLifecycleOwner) {
            map.updateVisibleMarkers(it)
        }

        bottomSheetData.observeNullable(this@MapFragment) {
            bottomSheetData.value = null
            setData(it.data)
            bottomSheet.state = BottomSheetBehavior.STATE_EXPANDED
        }
    }

    private fun setData(data: Data) {
        when (data.type) {
            ListType.ATTRACTION -> {
                bottomSheetBinding.title.text = (data.data as Attraction).name
            }
            ListType.INTEREST -> {
                bottomSheetBinding.title.text = (data.data as ShowPlace).name
            }
            ListType.SOUVENIRS,
            ListType.FOOD -> {
                bottomSheetBinding.title.text = (data.data as Place).name
            }
        }
        bottomSheetBinding.cardButton.setOnClickListener {
            findNavController().navigate(MapFragmentDirections.toDetails(data as Serializable))
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        map.onAttachMainMap(requireContext(), googleMap)
        map.moveTo(
            LatLng(56.243047, 93.526467), // Железногорск
            false,
            14f
        )
    }

    override fun onMarkerClick(marker: Marker): Boolean {
        map.moveTo(marker.position, true)
        val placeMarker = map.getPlaceById(marker.id)
        viewModel.handleMarkerClickListener(placeMarker)
        return true
    }
}