package com.avocado.digital_breakthrough_mobile.ui.map

import android.content.Context
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import timber.log.Timber

class MapController(
    private val onMarkerClickListener: GoogleMap.OnMarkerClickListener
) : Map {

    private lateinit var map: GoogleMap
    private val markersInfo = mutableMapOf<String, ParkMarker>()

    override fun moveTo(
        latLng: LatLng,
        isAnimate: Boolean,
        zoom: Float?
    ) {
        if (::map.isInitialized) {
            val realZoom = zoom ?: map.cameraPosition.zoom
            val cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, realZoom)
            when (isAnimate) {
                true -> map.animateCamera(cameraUpdate)
                false -> map.moveCamera(cameraUpdate)
            }
        }
    }

    override fun addMarker(marker: ParkMarker) {
        if (::map.isInitialized) {
            Timber.w("addMarker: ${marker.latLng.latitude}, ${marker.latLng.longitude}")
            val option = MarkerOptions()
                .icon(BitmapDescriptorFactory.fromResource(marker.avatarImageRes))
                .position(marker.latLng)
            val mapMarker = map.addMarker(option)
            markersInfo[mapMarker.id] = marker
        }
    }

    override fun clearMarkers() {
        if (::map.isInitialized) {
            Timber.w("clearMarkers")
            map.clear()
            markersInfo.clear()
        }
    }

    override fun updateVisibleMarkers(markers: Collection<ParkMarker>) {
        if (::map.isInitialized) {
            Timber.w("Update ${markers.size} markers")
            map.clear()
            markersInfo.clear()
            for (marker in markers) {
                addMarker(marker)
            }
        }
    }

    override fun getPlaceById(id: String): ParkMarker? {
        return markersInfo[id]
    }

    override fun onAttachMainMap(context: Context, googleMap: GoogleMap) {
        map = googleMap
        map.uiSettings.isMapToolbarEnabled = false
        map.uiSettings.isCompassEnabled = false
        map.setOnMarkerClickListener(onMarkerClickListener)
    }
}