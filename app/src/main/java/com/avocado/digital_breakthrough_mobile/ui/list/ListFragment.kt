package com.avocado.digital_breakthrough_mobile.ui.list

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.GridLayoutManager
import com.avocado.digital_breakthrough_mobile.databinding.FragmentListBinding
import com.avocado.digital_breakthrough_mobile.utils.observeNullable
import org.koin.androidx.viewmodel.ext.android.viewModel

class ListFragment : Fragment() {

    private val args by navArgs<ListFragmentArgs>()
    private val viewModel by viewModel<ListViewModel>()
    private lateinit var adapter: ListAdapter
    private lateinit var binding: FragmentListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.loadContent(args)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentListBinding.inflate(inflater)
        initUI()
        initObservers()
        return binding.root
    }

    private fun initUI() {
        binding.background.setBackgroundResource(args.type.backColorRes)
        binding.appbar.setBackgroundResource(args.type.backColorRes)
        binding.title.text = args.type.title

        binding.cardButton.setOnClickListener {
            findNavController().navigateUp()
        }

        adapter = ListAdapter {
            findNavController().navigate(ListFragmentDirections.toDetails(it))
        }
        val layoutManager = GridLayoutManager(context, 2)
        binding.recyclerView.layoutManager = layoutManager
        binding.recyclerView.adapter = adapter
    }

    private fun initObservers() = with(viewModel) {
        dataListLive.observeNullable(viewLifecycleOwner) {
            adapter.setData(it)
        }
    }
}