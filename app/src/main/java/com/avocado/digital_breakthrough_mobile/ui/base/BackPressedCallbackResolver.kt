package com.avocado.digital_breakthrough_mobile.ui.base

interface BackPressedCallbackResolver {

    fun addBackCallback(key: Int, callback: () -> Unit)
    fun removeBackCallback(key: Int)
    fun callParentCallback(key: Int)
}