package com.avocado.digital_breakthrough_mobile.ui.personal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.digital_breakthrough_mobile.databinding.FragmentPersonalBinding

class PersonalFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentPersonalBinding.inflate(inflater)
        binding.cardButton.setOnClickListener {
            findNavController().navigateUp()
        }
        return binding.root
    }
}