package com.avocado.digital_breakthrough_mobile.ui.webview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.avocado.digital_breakthrough_mobile.databinding.FragmentWebviewBinding
import com.avocado.digital_breakthrough_mobile.model.repository.PreferencesRepository
import com.avocado.digital_breakthrough_mobile.ui.list.ListType
import com.avocado.digital_breakthrough_mobile.utils.AppWebClient
import org.koin.core.KoinComponent
import org.koin.core.get

private const val SHARE_VAL = "share"

class WebViewFragment : Fragment(), KoinComponent {

    private val preferences: PreferencesRepository = get()
    private lateinit var binding: FragmentWebviewBinding
    private val args: WebViewFragmentArgs by navArgs()


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentWebviewBinding.inflate(inflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        WebView.setWebContentsDebuggingEnabled(true);
        binding.webView.apply {
            settings.javaScriptEnabled = true
            settings.domStorageEnabled = true
            webChromeClient = WebChromeClient()
            webViewClient = AppWebClient()
            addJavascriptInterface(JsInterface(), "AndroidFunction")
        }

        val url = args.url
        if (url == SHARE_VAL) {
            binding.webView.loadUrl("https://amc.avocadoonline.company/quest")
        } else {
            if (preferences.getIsFirstLoad()) {
                preferences.setIsFirstLoad(false)
                binding.webView.loadUrl("https://amc.avocadoonline.company/")
            } else {
                binding.webView.loadUrl("https://amc.avocadoonline.company/categories")
            }
        }
    }

    private inner class JsInterface {

        @JavascriptInterface
        fun openMapPage() {
            findNavController().navigate(WebViewFragmentDirections.toMapFragment())
        }

        @JavascriptInterface
        fun openAtractionsPage() {
            findNavController().navigate(WebViewFragmentDirections.toListFragment(ListType.ATTRACTION))
        }

        @JavascriptInterface
        fun openPersonalCollectionsPage() {
            findNavController().navigate(WebViewFragmentDirections.toPersonal())
        }

        @JavascriptInterface
        fun openInterestsPage() {
            findNavController().navigate(WebViewFragmentDirections.toListFragment(ListType.INTEREST))
        }

        @JavascriptInterface
        fun openSouvenirsPage() {
            findNavController().navigate(WebViewFragmentDirections.toListFragment(ListType.SOUVENIRS))
        }

        @JavascriptInterface
        fun openFoodPage() {
            findNavController().navigate(WebViewFragmentDirections.toListFragment(ListType.FOOD))
        }

        @JavascriptInterface
        fun openReplyUsPage() {
            findNavController().navigate(WebViewFragmentDirections.toWriteUs())
        }
    }
}