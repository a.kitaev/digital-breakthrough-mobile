package com.avocado.digital_breakthrough_mobile.ui.map

import android.content.Context
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng

interface Map {

    fun addMarker(marker: ParkMarker)
    fun clearMarkers()
    fun updateVisibleMarkers(markers: Collection<ParkMarker>)
    fun moveTo(latLng: LatLng, isAnimate: Boolean, zoom: Float? = null)
    fun onAttachMainMap(context: Context, googleMap: GoogleMap)
    fun getPlaceById(id: String): ParkMarker?
}