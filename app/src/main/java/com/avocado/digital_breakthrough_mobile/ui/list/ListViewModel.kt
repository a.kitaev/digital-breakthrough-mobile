package com.avocado.digital_breakthrough_mobile.ui.list

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.avocado.digital_breakthrough_mobile.model.data.DataList
import com.avocado.digital_breakthrough_mobile.model.interactor.MainInteractor
import kotlinx.coroutines.launch
import timber.log.Timber

class ListViewModel(
    private val mainInteractor: MainInteractor
) : ViewModel() {

    val dataListLive = MutableLiveData<DataList>()

    fun loadContent(args: ListFragmentArgs) {
        val type = args.type
        loadData(type)
    }

    private fun loadData(type: ListType) {
        viewModelScope.launch {
            try {
                val dataList = mainInteractor.loadDataByType(type)
                dataListLive.postValue(dataList)
            } catch (e: Exception) {
                Timber.e(e)
            }
        }
    }
}