package com.avocado.digital_breakthrough_mobile.ui.map

import androidx.annotation.DrawableRes
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.google.android.gms.maps.model.LatLng

class ParkMarker(
    val id: Long,
    val latLng: LatLng,
    @DrawableRes val avatarImageRes: Int,
    val data: Data
)