package com.avocado.digital_breakthrough_mobile.ui.map

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.avocado.digital_breakthrough_mobile.model.interactor.MainInteractor
import com.avocado.digital_breakthrough_mobile.utils.toParkMarker
import kotlinx.coroutines.launch
import timber.log.Timber

class MapViewModel(
    private val mainInteractor: MainInteractor
) : ViewModel() {

    val markerLiveData = MutableLiveData<List<ParkMarker>>()
    val bottomSheetData = MutableLiveData<ParkMarker>()

    fun loadContent() {
        viewModelScope.launch {
            loadData()
        }
    }

    private suspend fun loadData() {
        try {
            val dataHolder = mainInteractor.loadData()
            val markers = mutableListOf<ParkMarker>()
            dataHolder.forEach { dataList ->
                markers.addAll(dataList.dataList.map {
                    Data(
                        it,
                        dataList.type
                    ).toParkMarker()
                })
            }
            markerLiveData.postValue(markers)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }

    fun handleMarkerClickListener(parkMarker: ParkMarker?) {
        bottomSheetData.postValue(parkMarker)
    }
}