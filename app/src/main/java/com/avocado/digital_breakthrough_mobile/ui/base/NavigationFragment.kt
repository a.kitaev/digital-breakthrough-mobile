package com.avocado.digital_breakthrough_mobile.ui.base

import androidx.fragment.app.Fragment

abstract class NavigationFragment : Fragment() {

    protected lateinit var backCallback: () -> Unit
    private val callbackResolver by lazy {
        requireActivity() as BackPressedCallbackResolver
    }

    override fun onResume() {
        super.onResume()
        callbackResolver.addBackCallback(this.id, backCallback)
    }

    override fun onPause() {
        callbackResolver.removeBackCallback(this.id)
        super.onPause()
    }

    fun callParentBackPressedCallback() {
        callbackResolver.callParentCallback(this.id)
    }
}