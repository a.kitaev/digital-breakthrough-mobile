package com.avocado.digital_breakthrough_mobile.ui.details

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.avocado.digital_breakthrough_mobile.databinding.FragmentDetailsBinding
import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.avocado.digital_breakthrough_mobile.model.data.Place
import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace
import com.avocado.digital_breakthrough_mobile.model.providers.ImageProvider
import com.avocado.digital_breakthrough_mobile.ui.list.ListType
import com.avocado.digital_breakthrough_mobile.utils.setVisibility

class DetailsFragment : Fragment() {

    private val args by navArgs<DetailsFragmentArgs>()
    private lateinit var binding: FragmentDetailsBinding
    private val imageProvider = ImageProvider()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailsBinding.inflate(inflater)
        binding.cardButton.setOnClickListener {
            findNavController().navigateUp()
        }
        binding.buy.setOnClickListener {
            findNavController().navigate(DetailsFragmentDirections.toBuy())
        }
        val data = args.data as Data
        binding.buy.setVisibility(data.type == ListType.ATTRACTION)
        when (data.type) {
            ListType.ATTRACTION -> setByAttraction(data.data as Attraction)
            ListType.INTEREST -> setByShowPlace(data.data as ShowPlace)
            ListType.SOUVENIRS,
            ListType.FOOD -> setByPlace(data.data as Place)
        }
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    private fun setByAttraction(attraction: Attraction) {
        imageProvider.loadImage(attraction.urlPhoto, binding.image)
        binding.type.text = ListType.ATTRACTION.title
        binding.cardType.setCardBackgroundColor(
            ContextCompat.getColor(requireContext(), ListType.ATTRACTION.backColorRes)
        )
        binding.title.text = attraction.name
        binding.price.text = "${attraction.maxFullness}₽"
        binding.description.text = attraction.description
    }

    @SuppressLint("SetTextI18n")
    private fun setByShowPlace(showPlace: ShowPlace) {
        imageProvider.loadImage(showPlace.urlPhoto, binding.image)
        binding.type.text = ListType.INTEREST.title
        binding.cardType.setCardBackgroundColor(
            ContextCompat.getColor(requireContext(), ListType.INTEREST.backColorRes)
        )
        binding.title.text = showPlace.name
        binding.price.setVisibility(false)
        binding.description.text = showPlace.description
    }

    @SuppressLint("SetTextI18n")
    private fun setByPlace(place: Place) {
        imageProvider.loadImage(place.urlPhoto, binding.image)
        binding.type.text = ListType.FOOD.title
        binding.cardType.setCardBackgroundColor(
            ContextCompat.getColor(requireContext(), ListType.FOOD.backColorRes)
        )
        binding.title.text = place.name
        binding.price.text = "От ${place.maxFullness}₽"
        binding.description.text = place.description
    }
}