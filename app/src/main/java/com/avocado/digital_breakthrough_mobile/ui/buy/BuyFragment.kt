package com.avocado.digital_breakthrough_mobile.ui.buy

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.avocado.digital_breakthrough_mobile.databinding.FragmentBuyBinding

class BuyFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val binding = FragmentBuyBinding.inflate(inflater)
        binding.back.setOnClickListener {
            findNavController().navigate(BuyFragmentDirections.toWebViewFromBuy(url = "share"))
        }
        return binding.root
    }
}