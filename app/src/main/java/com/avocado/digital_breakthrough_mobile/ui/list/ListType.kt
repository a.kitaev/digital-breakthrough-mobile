package com.avocado.digital_breakthrough_mobile.ui.list

import androidx.annotation.ColorRes
import androidx.annotation.Keep
import com.avocado.digital_breakthrough_mobile.R

@Keep
enum class ListType(
    val title: String,
    @ColorRes val backColorRes: Int
) {
    ATTRACTION("Аттракционы", R.color.color_gold),
    INTEREST("Интересности", R.color.color_light_blue),
    FOOD("Фудкорты", R.color.color_red_orange),
    SOUVENIRS("Сувениры", R.color.color_green),
}