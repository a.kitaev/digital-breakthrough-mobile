package com.avocado.digital_breakthrough_mobile.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import com.avocado.digital_breakthrough_mobile.R
import com.avocado.digital_breakthrough_mobile.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), BackPressedCallbackResolver {

    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private val callbackList = mutableListOf<Pair<Int, () -> Unit>>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        navController = findNavController(R.id.nav_host_fragment)
    }

    override fun addBackCallback(key: Int, callback: () -> Unit) {
        callbackList.add(key to callback)
    }

    override fun removeBackCallback(key: Int) {
        callbackList.removeAll { it.first == key }
    }

    override fun callParentCallback(key: Int) {
        for (i in 0 until callbackList.size) {
            if (callbackList[i].first == key) {
                when (i) {
                    0 -> super.onBackPressed()
                    else -> callbackList[i - 1].second.invoke()
                }
            }
        }
    }

    override fun onBackPressed() {
        if (callbackList.isEmpty()) {
            super.onBackPressed()
        } else {
            callbackList.last().second.invoke()
        }
    }
}