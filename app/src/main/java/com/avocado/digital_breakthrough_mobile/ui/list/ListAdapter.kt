package com.avocado.digital_breakthrough_mobile.ui.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.avocado.digital_breakthrough_mobile.databinding.ItemListItemBinding
import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.avocado.digital_breakthrough_mobile.model.data.DataList
import com.avocado.digital_breakthrough_mobile.model.data.Place
import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace
import com.avocado.digital_breakthrough_mobile.model.providers.ImageProvider

class ListAdapter(
    private val callback: (Data) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val imageProvider = ImageProvider()
    private val items = ArrayList<Item>()

    fun setData(dataList: DataList) {
        synchronized(items) {
            items.clear()
            val localItems = when (dataList.type) {
                ListType.ATTRACTION -> dataList.dataList.map { AttractionItem(it as Attraction) }
                ListType.INTEREST -> dataList.dataList.map { InterestItem(it as ShowPlace) }
                ListType.FOOD,
                ListType.SOUVENIRS -> dataList.dataList.map { PlaceItem(it as Place) }
            }
            items.addAll(localItems)
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListItemBinding.inflate(inflater, parent, false)
        return when (viewType) {
            TYPE_ATTRACTION -> AttractionHolder(binding)
            TYPE_FOOD_SOUVENIRS -> PlaceHolder(binding)
            TYPE_INTEREST -> ShowPlaceHolder(binding)
            else -> throw IllegalStateException("viewType not implemented")
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = items[position]
        when (item.viewType) {
            TYPE_ATTRACTION -> (holder as AttractionHolder).set((item as AttractionItem).attraction)
            TYPE_FOOD_SOUVENIRS -> (holder as PlaceHolder).set((item as PlaceItem).place)
            TYPE_INTEREST -> (holder as ShowPlaceHolder).set((item as InterestItem).interest)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].viewType
    }

    override fun getItemCount(): Int {
        return items.size
    }

    inner class AttractionHolder(
        private val binding: ItemListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun set(attraction: Attraction) {
            binding.title.text = attraction.name
            binding.price.text = "${attraction.maxFullness}₽"
            imageProvider.loadImage(attraction.urlPhoto, binding.image)
            binding.backgroundButton.setOnClickListener {
                callback.invoke(Data(attraction, ListType.ATTRACTION))
            }
        }
    }

    inner class ShowPlaceHolder(
        private val binding: ItemListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun set(showPlace: ShowPlace) {
            binding.title.text = showPlace.name
            binding.price.visibility = View.GONE
            imageProvider.loadImage(showPlace.urlPhoto, binding.image)
            binding.backgroundButton.setOnClickListener {
                callback.invoke(Data(showPlace, ListType.INTEREST))
            }
        }
    }

    inner class PlaceHolder(
        private val binding: ItemListItemBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun set(place: Place) {
            binding.title.text = place.name
            binding.price.text = "От ${place.maxFullness}₽"
            imageProvider.loadImage(place.urlPhoto, binding.image)
            binding.backgroundButton.setOnClickListener {
                callback.invoke(Data(place, ListType.FOOD))
            }
        }
    }

    interface Item {
        val viewType: Int
    }

    class AttractionItem(val attraction: Attraction) : Item {
        override val viewType: Int
            get() = TYPE_ATTRACTION
    }

    class InterestItem(val interest: ShowPlace) : Item {
        override val viewType: Int
            get() = TYPE_INTEREST
    }

    class PlaceItem(val place: Place) : Item {
        override val viewType: Int
            get() = TYPE_FOOD_SOUVENIRS
    }

    companion object {
        private const val TYPE_ATTRACTION = 1
        private const val TYPE_INTEREST = 2
        private const val TYPE_FOOD_SOUVENIRS = 3
    }
}