package com.avocado.digital_breakthrough_mobile.di

import com.avocado.digital_breakthrough_mobile.ProjectApplication
import com.avocado.digital_breakthrough_mobile.api.provideAttractionsCall
import com.avocado.digital_breakthrough_mobile.api.provideOkHttpClient
import com.avocado.digital_breakthrough_mobile.api.providePacksCall
import com.avocado.digital_breakthrough_mobile.api.providePlacesCall
import com.avocado.digital_breakthrough_mobile.api.provideRetrofit
import com.avocado.digital_breakthrough_mobile.api.provideShowPlacesCall
import com.avocado.digital_breakthrough_mobile.model.interactor.MainInteractor
import com.avocado.digital_breakthrough_mobile.model.providers.ImageProvider
import com.avocado.digital_breakthrough_mobile.model.providers.ResourceProvider
import com.avocado.digital_breakthrough_mobile.model.repository.MainRepository
import com.avocado.digital_breakthrough_mobile.model.repository.PreferencesRepository
import com.avocado.digital_breakthrough_mobile.ui.list.ListViewModel
import com.avocado.digital_breakthrough_mobile.ui.map.MapViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val repositoryModule = module(override = true) {
    single { PreferencesRepository() }
    factory { provideOkHttpClient() }
    single { provideRetrofit(get()) }

    single { providePlacesCall(get()) }
    single { providePacksCall(get()) }
    single { provideAttractionsCall(get()) }
    single { provideShowPlacesCall(get()) }

    single { MainRepository(get(), get(), get(), get()) }
}

val interactorModule = module(override = true) {
    single { MainInteractor(get()) }
}

val providerModule = module(override = true) {
    single { ResourceProvider(ProjectApplication.appInstance.applicationContext) }
    single { ImageProvider() }
}

val viewModelModule = module(override = true) {
    viewModel { MapViewModel(get()) }
    viewModel { ListViewModel(get()) }
}