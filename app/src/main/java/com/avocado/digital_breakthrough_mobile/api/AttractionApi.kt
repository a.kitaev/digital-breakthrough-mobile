package com.avocado.digital_breakthrough_mobile.api

import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import retrofit2.http.GET

interface AttractionApi {

    @GET("attraction/getAll")
    suspend fun getAttractions(): AttractionResponse

    data class AttractionResponse(
        val response: List<Attraction>
    )
}