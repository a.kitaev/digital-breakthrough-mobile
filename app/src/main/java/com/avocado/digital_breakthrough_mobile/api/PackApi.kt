package com.avocado.digital_breakthrough_mobile.api

import com.avocado.digital_breakthrough_mobile.model.data.Pack
import retrofit2.http.GET

interface PackApi {

    @GET("pack/getAll")
    suspend fun getPacks(): PackResponse

    data class PackResponse(
        val response: List<Pack>
    )
}