package com.avocado.digital_breakthrough_mobile.api

import com.avocado.digital_breakthrough_mobile.model.data.Place
import retrofit2.http.GET

interface PlacesApi {

    @GET("place/getAll")
    suspend fun getPlaces(): PlaceResponse

    data class PlaceResponse(
        val response:List<Place>
    )
}