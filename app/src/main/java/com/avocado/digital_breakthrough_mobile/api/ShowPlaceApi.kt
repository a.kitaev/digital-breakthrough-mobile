package com.avocado.digital_breakthrough_mobile.api

import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace
import retrofit2.http.GET

interface ShowPlaceApi {

    @GET("showplace/getAll")
    suspend fun getShowPlaces(): ShowPlaceResponse

    data class ShowPlaceResponse(
        val response: List<ShowPlace>
    )
}