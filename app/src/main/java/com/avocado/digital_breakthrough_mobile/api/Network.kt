package com.avocado.digital_breakthrough_mobile.api

import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

const val BASE_URL = "https://amcbackend.avocadoonline.company/"

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder()
        .baseUrl(BASE_URL)
        .client(okHttpClient)
        .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(): OkHttpClient {
    return OkHttpClient().newBuilder().build()
}

fun providePlacesCall(retrofit: Retrofit): PlacesApi = retrofit
    .create(PlacesApi::class.java)

fun provideShowPlacesCall(retrofit: Retrofit): ShowPlaceApi = retrofit
    .create(ShowPlaceApi::class.java)

fun providePacksCall(retrofit: Retrofit): PackApi = retrofit
    .create(PackApi::class.java)

fun provideAttractionsCall(retrofit: Retrofit): AttractionApi = retrofit
    .create(AttractionApi::class.java)