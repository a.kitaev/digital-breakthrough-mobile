package com.avocado.digital_breakthrough_mobile.model.providers

import android.widget.ImageView
import com.squareup.picasso.Picasso
import timber.log.Timber

class ImageProvider {

    fun loadImage(url: String, imageView: ImageView) {
        try {
            Picasso.get()
                .load(url)
                .into(imageView)
        } catch (e: Exception) {
            Timber.e(e)
        }
    }
}