package com.avocado.digital_breakthrough_mobile.model.data

import com.avocado.digital_breakthrough_mobile.ui.list.ListType
import java.io.Serializable

class DataList(
    val dataList: List<Any>,
    val type: ListType
) : Serializable

class Data(
    val data: Any,
    val type: ListType
) : Serializable