package com.avocado.digital_breakthrough_mobile.model.repository

import com.avocado.digital_breakthrough_mobile.ProjectApplication
import android.content.Context
import android.content.SharedPreferences
import com.avocado.digital_breakthrough_mobile.utils.KeyStore

class PreferencesRepository {

    private val preferences: SharedPreferences by lazy {
        ProjectApplication.appInstance.applicationContext.getSharedPreferences(
            KeyStore.SHARED_PREFERENCES_KEY,
            Context.MODE_PRIVATE
        )
    }

    fun getIsFirstLoad(): Boolean {
        return preferences.getBoolean("IS_FIRST_LOAD", true)
    }

    fun setIsFirstLoad(isFirst: Boolean) {
        preferences.edit().putBoolean("IS_FIRST_LOAD", isFirst).apply()
    }

    fun clearPreferences() {
        preferences.edit()
            .clear()
            .apply()
    }
}