package com.avocado.digital_breakthrough_mobile.model.data

data class Pack(
    var id: String = "",
    var user: String = "",
    var message: String = "",
    var attractions: List<Attraction> = listOf(),
    var showplaces: List<ShowPlace> = listOf(),
    var places: List<Place> = listOf(),
    var cost: Int = 0
)
