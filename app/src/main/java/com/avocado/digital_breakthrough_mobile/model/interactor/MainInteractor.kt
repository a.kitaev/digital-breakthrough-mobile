package com.avocado.digital_breakthrough_mobile.model.interactor

import com.avocado.digital_breakthrough_mobile.model.data.DataList
import com.avocado.digital_breakthrough_mobile.model.repository.MainRepository
import com.avocado.digital_breakthrough_mobile.ui.list.ListType

class MainInteractor(
    private val mainRepository: MainRepository
) {

    suspend fun loadDataByType(type: ListType) = when (type) {
        ListType.ATTRACTION -> DataList(
            mainRepository.getAttractions(),
            ListType.ATTRACTION
        )
        ListType.INTEREST -> DataList(
            mainRepository.getShowPlaces(),
            ListType.INTEREST
        )
        ListType.FOOD -> DataList(
            mainRepository.getPlaces(),
            ListType.FOOD
        )
        ListType.SOUVENIRS -> DataList(
            mainRepository.getPlaces(),
            ListType.SOUVENIRS
        )
    }

    suspend fun loadData(): List<DataList> {
        return listOf(
            DataList(mainRepository.getAttractions(), ListType.ATTRACTION),
            DataList(mainRepository.getShowPlaces(), ListType.INTEREST),
            DataList(mainRepository.getPlaces(), ListType.FOOD)
        )
    }
}