package com.avocado.digital_breakthrough_mobile.model.data

data class Place(
    var name: String = "",
    var id: String = "",
    var urlPhoto: String = "",
    var description: String = "",
    var lat: Double = 0.0,
    var lng: Double = 0.0,
    var currentFullness: Int = 0,
    var maxFullness: Int = 0,
    var workingHours: String = "",
    var queueId: String = ""
)
