package com.avocado.digital_breakthrough_mobile.model.repository

import com.avocado.digital_breakthrough_mobile.api.AttractionApi
import com.avocado.digital_breakthrough_mobile.api.PackApi
import com.avocado.digital_breakthrough_mobile.api.PlacesApi
import com.avocado.digital_breakthrough_mobile.api.ShowPlaceApi
import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import com.avocado.digital_breakthrough_mobile.model.data.Pack
import com.avocado.digital_breakthrough_mobile.model.data.Place
import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace

class MainRepository(
    private val attractionApi: AttractionApi,
    private val packApi: PackApi,
    private val placesApi: PlacesApi,
    private val showPlaceApi: ShowPlaceApi
) {

    suspend fun getAttractions(): List<Attraction> {
        return attractionApi.getAttractions().response
    }

    suspend fun getPacks(): List<Pack> {
        return packApi.getPacks().response
    }

    suspend fun getPlaces(): List<Place> {
        return placesApi.getPlaces().response
    }

    suspend fun getShowPlaces(): List<ShowPlace> {
        return showPlaceApi.getShowPlaces().response
    }
}