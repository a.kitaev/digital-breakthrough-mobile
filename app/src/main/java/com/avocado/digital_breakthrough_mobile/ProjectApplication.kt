package com.avocado.digital_breakthrough_mobile

import android.app.Application
import com.avocado.digital_breakthrough_mobile.di.interactorModule
import com.avocado.digital_breakthrough_mobile.di.providerModule
import com.avocado.digital_breakthrough_mobile.di.repositoryModule
import com.avocado.digital_breakthrough_mobile.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ProjectApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        appInstance = this
        startKoin {
            androidContext(this@ProjectApplication)
            modules(
                listOf(
                    repositoryModule,
                    interactorModule,
                    providerModule,
                    viewModelModule
                )
            )
        }
    }

    companion object {

        lateinit var appInstance: ProjectApplication
            private set
    }
}