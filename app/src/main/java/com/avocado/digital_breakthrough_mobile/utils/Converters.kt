package com.avocado.digital_breakthrough_mobile.utils

import com.avocado.digital_breakthrough_mobile.R
import com.avocado.digital_breakthrough_mobile.model.data.Attraction
import com.avocado.digital_breakthrough_mobile.model.data.Data
import com.avocado.digital_breakthrough_mobile.model.data.Place
import com.avocado.digital_breakthrough_mobile.model.data.ShowPlace
import com.avocado.digital_breakthrough_mobile.ui.list.ListType
import com.avocado.digital_breakthrough_mobile.ui.map.ParkMarker
import com.google.android.gms.maps.model.LatLng
import kotlin.random.Random

fun Data.toParkMarker(): ParkMarker {
    return when (this.type) {
        ListType.ATTRACTION -> {
            val obj = this.data as Attraction
            ParkMarker(
                id = Random.nextLong(),
                latLng = LatLng(obj.lng, obj.lat),
                avatarImageRes = R.drawable.ic_attraction,
                data = this
            )
        }
        ListType.INTEREST -> {
            val obj = this.data as ShowPlace
            ParkMarker(
                id = Random.nextLong(),
                latLng = LatLng(obj.lng, obj.lat),
                avatarImageRes = R.drawable.ic_interests,
                data = this
            )
        }
        ListType.SOUVENIRS,
        ListType.FOOD -> {
            val obj = this.data as Place
            ParkMarker(
                id = Random.nextLong(),
                latLng = LatLng(obj.lng, obj.lat),
                avatarImageRes = R.drawable.ic_food,
                data = this
            )
        }
    }
}